﻿using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Moq;
using Moq.Protected;
using NUnit.Framework;
using StarWarsApi.Swapi.Model.Resources;
using StarWarsApi.Swapi.Services;
using StarWarsApi.Swapi.Services.Impl;

namespace StarWarsApi.Swapi.UnitTesting;

public partial class SwapiServiceImplTest
{
  [Test]
  public async Task TestGetPeopleByIdAsyncReturnsPeopleObject()
  {
    Mock<HttpMessageHandler> httpMessageHandlerMock = new();
    httpMessageHandlerMock.Protected()
      .Setup<Task<HttpResponseMessage>>("SendAsync", ItExpr.IsAny<HttpRequestMessage>(), ItExpr.IsAny<CancellationToken>())
      .ReturnsAsync(new HttpResponseMessage { StatusCode = HttpStatusCode.OK, Content = new StringContent(MockPeople1Json) });

    Mock<IHttpClientFactory> httpClientFactoryMock = new();
    httpClientFactoryMock
      .Setup(factory => factory.CreateClient(It.IsAny<string>()))
      .Returns(new HttpClient(httpMessageHandlerMock.Object));

    ISwapiResourceService<People> swapiPeopleService = new SwapiPeopleServiceImpl(httpClientFactoryMock.Object);

    People? luke = await swapiPeopleService.GetByIdAsync(1);

    Assert.IsNotNull(luke);
    Assert.AreEqual("Luke Skywalker", luke!.Name);
    Assert.AreEqual("172", luke.Height);
    Assert.AreEqual("https://swapi.test/api/people/1/", luke.Url);
  }

  [Test]
  public async Task TestGetPeopleByIdAsyncReturnsNullOn404()
  {
    Mock<HttpMessageHandler> httpMessageHandlerMock = new();
    httpMessageHandlerMock.Protected()
      .Setup<Task<HttpResponseMessage>>("SendAsync", ItExpr.IsAny<HttpRequestMessage>(), ItExpr.IsAny<CancellationToken>())
      .ReturnsAsync(new HttpResponseMessage { StatusCode = HttpStatusCode.NotFound });

    Mock<IHttpClientFactory> httpClientFactoryMock = new();
    httpClientFactoryMock
      .Setup(factory => factory.CreateClient(It.IsAny<string>()))
      .Returns(new HttpClient(httpMessageHandlerMock.Object));

    ISwapiResourceService<People> swapiPeopleService = new SwapiPeopleServiceImpl(httpClientFactoryMock.Object);

    People? people = await swapiPeopleService.GetByIdAsync(1);

    Assert.IsNull(people);
  }

  [Test]
  public void TestGetPeopleByIdAsyncThrowsServiceUnavailableExceptionFromHttpRequestException()
  {
    Mock<HttpMessageHandler> httpMessageHandlerMock = new();
    httpMessageHandlerMock.Protected()
      .Setup<Task<HttpResponseMessage>>("SendAsync", ItExpr.IsAny<HttpRequestMessage>(), ItExpr.IsAny<CancellationToken>())
      .ReturnsAsync(new HttpResponseMessage { StatusCode = HttpStatusCode.InternalServerError });

    Mock<IHttpClientFactory> httpClientFactoryMock = new();
    httpClientFactoryMock
      .Setup(factory => factory.CreateClient(It.IsAny<string>()))
      .Returns(new HttpClient(httpMessageHandlerMock.Object));

    ISwapiResourceService<People> swapiPeopleService = new SwapiPeopleServiceImpl(httpClientFactoryMock.Object);

    ServiceUnavailableException? exception = Assert.ThrowsAsync<ServiceUnavailableException>(() => swapiPeopleService.GetByIdAsync(1));
    Assert.IsInstanceOf<HttpRequestException>(exception?.InnerException);
  }
}
