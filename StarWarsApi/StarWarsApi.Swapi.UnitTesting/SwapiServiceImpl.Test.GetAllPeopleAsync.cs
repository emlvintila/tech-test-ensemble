﻿using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Moq;
using Moq.Protected;
using NUnit.Framework;
using StarWarsApi.Swapi.Model;
using StarWarsApi.Swapi.Model.Resources;
using StarWarsApi.Swapi.Services;
using StarWarsApi.Swapi.Services.Impl;

namespace StarWarsApi.Swapi.UnitTesting;

[TestFixture]
public partial class SwapiServiceImplTest
{
  [Test]
  public async Task TestGetAllPeopleReturnsAllPeopleObject()
  {
    Mock<HttpMessageHandler> httpMessageHandlerMock = new();
    httpMessageHandlerMock.Protected()
      .Setup<Task<HttpResponseMessage>>("SendAsync", ItExpr.IsAny<HttpRequestMessage>(), ItExpr.IsAny<CancellationToken>())
      .ReturnsAsync(new HttpResponseMessage { StatusCode = HttpStatusCode.OK, Content = new StringContent(MockAllPeopleJson) });

    Mock<IHttpClientFactory> httpClientFactoryMock = new();
    httpClientFactoryMock
      .Setup(factory => factory.CreateClient(It.IsAny<string>()))
      .Returns(new HttpClient(httpMessageHandlerMock.Object));

    ISwapiResourceService<People> swapiPeopleService = new SwapiPeopleServiceImpl(httpClientFactoryMock.Object);

    ISwapiResultsList<People> peopleList = await swapiPeopleService.GetPageAsync();

    Assert.AreEqual(82, peopleList.Count);
    Assert.AreEqual(10, peopleList.Results.Count());
    Assert.IsNotNull(peopleList.Next);
    Assert.IsNull(peopleList.Previous);
  }

  [Test]
  public void TestGetAllPeopleAsyncThrowsServiceUnavailableExceptionFromHttpRequestException()
  {
    Mock<HttpMessageHandler> httpMessageHandlerMock = new();
    httpMessageHandlerMock.Protected()
      .Setup<Task<HttpResponseMessage>>("SendAsync", ItExpr.IsAny<HttpRequestMessage>(), ItExpr.IsAny<CancellationToken>())
      .ReturnsAsync(new HttpResponseMessage { StatusCode = HttpStatusCode.InternalServerError });

    Mock<IHttpClientFactory> httpClientFactoryMock = new();
    httpClientFactoryMock
      .Setup(factory => factory.CreateClient(It.IsAny<string>()))
      .Returns(new HttpClient(httpMessageHandlerMock.Object));

    ISwapiResourceService<People> swapiPeopleService = new SwapiPeopleServiceImpl(httpClientFactoryMock.Object);

    ServiceUnavailableException? exception = Assert.ThrowsAsync<ServiceUnavailableException>(() => swapiPeopleService.GetPageAsync());
    Assert.IsInstanceOf<HttpRequestException>(exception?.InnerException);
  }
}
