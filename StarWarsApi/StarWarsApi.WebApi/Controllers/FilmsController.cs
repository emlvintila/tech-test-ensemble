﻿using AutoMapper;
using StarWarsApi.Swapi.Model.Resources;
using StarWarsApi.Swapi.Services;
using StarWarsApi.WebApi.Model;

namespace StarWarsApi.WebApi.Controllers;

public class FilmsController : StarWarsApiControllerBase<Film, FilmDto>
{
  public FilmsController(ISwapiResourceService<Film> swapiResourceService, IMapper mapper) : base(swapiResourceService, mapper) { }
}
