﻿using AutoMapper;
using StarWarsApi.Swapi.Model.Resources;
using StarWarsApi.Swapi.Services;
using StarWarsApi.WebApi.Model;

namespace StarWarsApi.WebApi.Controllers;

public class StarshipsController : StarWarsApiControllerBase<Starship, StarshipDto>
{
  public StarshipsController(ISwapiResourceService<Starship> swapiResourceService, IMapper mapper) : base(swapiResourceService, mapper) { }
}
