﻿using AutoMapper;
using StarWarsApi.Swapi.Model.Resources;
using StarWarsApi.Swapi.Services;
using StarWarsApi.WebApi.Model;

namespace StarWarsApi.WebApi.Controllers;

public class PeopleController : StarWarsApiControllerBase<People, PeopleDto>
{
  public PeopleController(ISwapiResourceService<People> swapiResourceService, IMapper mapper) : base(swapiResourceService, mapper) { }
}
