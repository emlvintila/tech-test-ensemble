﻿using Microsoft.AspNetCore.Mvc;
using StarWarsApi.Swapi;
using StarWarsApi.WebApi.Model;
using StarWarsApi.WebApi.Services;

namespace StarWarsApi.WebApi.Controllers;

[Route("api/films-ex")]
public class FilmsExController : ControllerBase
{
  private readonly IResourceExService<FilmExDto> filmExService;

  public FilmsExController(IResourceExService<FilmExDto> filmExService) => this.filmExService = filmExService;

  [Route("{id:int}")]
  [HttpGet]
  public async Task<ActionResult<FilmExDto>> GetById(int id, CancellationToken cancellationToken = default)
  {
    FilmExDto? filmEx;
    try
    {
      filmEx = await filmExService.GetByIdAsync(id, cancellationToken);
    }
    catch (Exception exception) when (exception is ServiceUnavailableException)
    {
      return Problem(Resources.ErrorMessageUpstreamServiceUnavailable, statusCode: StatusCodes.Status503ServiceUnavailable);
    }

    if (filmEx is null)
      return NotFound();

    return Ok(filmEx);
  }
}
