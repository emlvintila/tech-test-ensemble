﻿using AutoMapper;
using StarWarsApi.Swapi.Model.Resources;
using StarWarsApi.Swapi.Services;
using StarWarsApi.WebApi.Model;

namespace StarWarsApi.WebApi.Controllers;

public class VehiclesController : StarWarsApiControllerBase<Vehicle, VehicleDto>
{
  public VehiclesController(ISwapiResourceService<Vehicle> swapiResourceService, IMapper mapper) : base(swapiResourceService, mapper) { }
}
