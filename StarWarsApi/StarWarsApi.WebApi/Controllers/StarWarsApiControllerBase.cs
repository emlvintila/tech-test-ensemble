﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using StarWarsApi.Swapi;
using StarWarsApi.Swapi.Services;

namespace StarWarsApi.WebApi.Controllers;

[Route("api/[controller]")]
public abstract class StarWarsApiControllerBase<TSwapiResource, TDomainResource> : ControllerBase
{
  private readonly IMapper mapper;
  private readonly ISwapiResourceService<TSwapiResource> swapiResourceService;

  protected StarWarsApiControllerBase(ISwapiResourceService<TSwapiResource> swapiResourceService, IMapper mapper)
  {
    this.swapiResourceService = swapiResourceService;
    this.mapper = mapper;
  }

  [ResponseCache(Duration = 60, Location = ResponseCacheLocation.Any, VaryByQueryKeys = new[] { "search" })]
  [HttpGet("all")]
  public async Task<ActionResult<IEnumerable<TDomainResource>>> GetAll([FromQuery] string? search = null, CancellationToken cancellationToken = default)
  {
    try
    {
      IEnumerable<TDomainResource> domainResources = (await swapiResourceService.GetAllAsync(searchQuery: search?.Trim(), cancellationToken: cancellationToken))
        .SelectMany(list => list.Results)
        .Select(mapper.Map<TSwapiResource, TDomainResource>);

      return Ok(domainResources);
    }
    catch (Exception exception) when (exception is ServiceUnavailableException)
    {
      return Problem(Resources.ErrorMessageUpstreamServiceUnavailable, statusCode: StatusCodes.Status503ServiceUnavailable);
    }
  }

  [ResponseCache(Duration = 60, Location = ResponseCacheLocation.Any,
    VaryByQueryKeys = new[]
    {
      "page",
      "search"
    })]
  [HttpGet]
  public async Task<ActionResult<IEnumerable<TDomainResource>>> Get([FromQuery] int page = 1, [FromQuery] string? search = null,
    CancellationToken cancellationToken = default)
  {
    try
    {
      IEnumerable<TDomainResource> domainResource = (await swapiResourceService.GetPageAsync(page, search?.Trim(), cancellationToken))
        .Results
        .Select(mapper.Map<TSwapiResource, TDomainResource>);

      return Ok(domainResource);
    }
    catch (Exception exception) when (exception is ServiceUnavailableException)
    {
      return Problem(Resources.ErrorMessageUpstreamServiceUnavailable, statusCode: StatusCodes.Status503ServiceUnavailable);
    }
  }

  [ResponseCache(Duration = 60, Location = ResponseCacheLocation.Any)]
  [HttpGet("{id:int}")]
  public async Task<ActionResult<TDomainResource>> GetById(int id, CancellationToken cancellationToken = default)
  {
    TSwapiResource? swapiResource;
    try
    {
      swapiResource = await swapiResourceService.GetByIdAsync(id, cancellationToken);
    }
    catch (Exception exception) when (exception is ServiceUnavailableException)
    {
      return Problem(Resources.ErrorMessageUpstreamServiceUnavailable, statusCode: StatusCodes.Status503ServiceUnavailable);
    }

    if (swapiResource is null)
      return NotFound();

    return Ok(mapper.Map<TSwapiResource, TDomainResource>(swapiResource));
  }
}
