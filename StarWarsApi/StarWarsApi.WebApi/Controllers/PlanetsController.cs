﻿using AutoMapper;
using StarWarsApi.Swapi.Model.Resources;
using StarWarsApi.Swapi.Services;
using StarWarsApi.WebApi.Model;

namespace StarWarsApi.WebApi.Controllers;

public class PlanetsController : StarWarsApiControllerBase<Planet, PlanetDto>
{
  public PlanetsController(ISwapiResourceService<Planet> swapiResourceService, IMapper mapper) : base(swapiResourceService, mapper) { }
}
