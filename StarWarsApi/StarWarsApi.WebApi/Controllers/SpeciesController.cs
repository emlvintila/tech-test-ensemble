﻿using AutoMapper;
using StarWarsApi.Swapi.Model.Resources;
using StarWarsApi.Swapi.Services;
using StarWarsApi.WebApi.Model;

namespace StarWarsApi.WebApi.Controllers;

public class SpeciesController : StarWarsApiControllerBase<Species, SpeciesDto>
{
  public SpeciesController(ISwapiResourceService<Species> swapiResourceService, IMapper mapper) : base(swapiResourceService, mapper) { }
}
