using System.Diagnostics.CodeAnalysis;
using System.Reflection;
using StarWarsApi.Swapi.Model.Resources;
using StarWarsApi.Swapi.Services;
using StarWarsApi.Swapi.Services.Impl;
using StarWarsApi.WebApi.Model;
using StarWarsApi.WebApi.Services;
using StarWarsApi.WebApi.Services.Impl;

WebApplicationBuilder builder = WebApplication.CreateBuilder(args);

builder.Services.Configure<RouteOptions>(options => options.LowercaseUrls = true);

builder.Services.AddResponseCaching();

// Response compression should be handled by the reverse proxy (such as nginx), not by Kestrel itself
// builder.Services.AddResponseCompression();

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

builder.Services.AddHttpClient();
builder.Services.AddScoped<ISwapiResourceService<People>, SwapiPeopleServiceImpl>();
builder.Services.AddScoped<ISwapiResourceService<Film>, SwapiFilmServiceImpl>();
builder.Services.AddScoped<ISwapiResourceService<Starship>, SwapiStarshipServiceImpl>();
builder.Services.AddScoped<ISwapiResourceService<Vehicle>, SwapiVehicleServiceImpl>();
builder.Services.AddScoped<ISwapiResourceService<Species>, SwapiSpeciesServiceImpl>();
builder.Services.AddScoped<ISwapiResourceService<Planet>, SwapiPlanetServiceImpl>();
builder.Services.AddScoped<IResourceExService<FilmExDto>, FilmsExService>();

builder.Services.AddAutoMapper(config => { config.AddMaps(Assembly.GetExecutingAssembly()); });

WebApplication app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
  app.UseSwagger();
  app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.UseResponseCaching();

app.MapControllers();

app.Run();

[SuppressMessage("Design", "CA1050:Declare types in namespaces")]
[SuppressMessage("ReSharper", "ClassNeverInstantiated.Global")]
public partial class Program { }
