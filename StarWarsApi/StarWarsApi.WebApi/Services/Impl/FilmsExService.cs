﻿using AutoMapper;
using StarWarsApi.Swapi.Model.Resources;
using StarWarsApi.Swapi.Services;
using StarWarsApi.WebApi.Model;

namespace StarWarsApi.WebApi.Services.Impl;

public class FilmsExService : ResourceExServiceBase<FilmExDto, FilmDto, Film>
{
  public FilmsExService(IMapper mapper, IServiceProvider serviceProvider, ISwapiResourceService<Film> swapiResourceService) : base(mapper, serviceProvider,
    swapiResourceService) { }

  protected override IEnumerable<Func<Film, (Type SwapiSubresourceType, Type DomainSubresourceType, string[] SwapiSubresourceUris)>> GetSubResources() =>
    new Func<Film, (Type SwapiSubresourceType, Type DomainSubresourceType, string[] SwapiSubresourceUris)>[]
    {
      film => (typeof(People), typeof(PeopleDto), film.Characters),
      film => (typeof(Planet), typeof(PlanetDto), film.Planets),
      film => (typeof(Species), typeof(SpeciesDto), film.Species),
      film => (typeof(Starship), typeof(StarshipDto), film.Starships),
      film => (typeof(Vehicle), typeof(VehicleDto), film.Vehicles)
    };
}
