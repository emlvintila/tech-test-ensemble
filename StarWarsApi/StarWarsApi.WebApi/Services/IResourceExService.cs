﻿namespace StarWarsApi.WebApi.Services;

public interface IResourceExService<TSwapiResourceEx>
{
  Task<TSwapiResourceEx?> GetByIdAsync(int id, CancellationToken cancellationToken = default);
}
