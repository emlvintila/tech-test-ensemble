﻿using System.Reflection;
using StarWarsApi.Swapi.Services;

namespace StarWarsApi.WebApi.Services;

internal static class ResourceExServiceTypes
{
  /// <summary>
  ///   TResult[] Task.WhenAll[TResult](IEnumerable[Task[TResult]] tasks)
  /// </summary>
  public static readonly MethodInfo TaskWhenAllMethodInfoUnbound = typeof(Task)
    .GetMethods(BindingFlags.Public | BindingFlags.Static)
    .Where(method => method.Name == nameof(Task.WhenAll))
    .Where(method => method.IsGenericMethod)
    .Single(method => method.GetParameters().Single().ParameterType.IsGenericType);

  public static readonly MethodInfo EnumerableCastMethodInfoUnbound = typeof(Enumerable)
    .GetMethod(nameof(Enumerable.Cast), BindingFlags.Public | BindingFlags.Static)!;

  public static readonly Type SwapiResourceServiceTypeUnbound = typeof(ISwapiResourceService<>);
  public static readonly Type TaskTypeUnbound = typeof(Task<>);

  // ReSharper disable once InconsistentNaming
  public static readonly Type IEnumerableTypeUnbound = typeof(IEnumerable<>);
}
