﻿using System.Collections.Immutable;
using System.Reflection;
using AutoMapper;
using StarWarsApi.Swapi.Services;
using StarWarsApi.WebApi.Utils;

namespace StarWarsApi.WebApi.Services;

public abstract class ResourceExServiceBase<TDomainResourceEx, TDomainResource, TSwapiResource> : IResourceExService<TDomainResourceEx>
  where TDomainResourceEx : new()
{
  private static readonly Type SwapiResourceType = typeof(TSwapiResource);
  private static readonly Type DomainResourceType = typeof(TDomainResource);
  private static readonly Type DomainResourceExType = typeof(TDomainResourceEx);

  protected readonly IMapper Mapper;
  protected readonly IServiceProvider ServiceProvider;
  protected readonly ISwapiResourceService<TSwapiResource> SwapiResourceService;

  protected ResourceExServiceBase(IMapper mapper, IServiceProvider serviceProvider,
    ISwapiResourceService<TSwapiResource> swapiResourceService)
  {
    Mapper = mapper;
    ServiceProvider = serviceProvider;
    SwapiResourceService = swapiResourceService;
  }

  public async Task<TDomainResourceEx?> GetByIdAsync(int id, CancellationToken cancellationToken = default)
  {
    await using AsyncServiceScope asyncServiceScope = ServiceProvider.CreateAsyncScope();

    // get the swapi resource
    TSwapiResource? swapiResource = await SwapiResourceService.GetByIdAsync(id, cancellationToken);
    if (swapiResource is null)
      return default;

    // map it to the domain resource
    object domainResource = Mapper.Map(swapiResource, SwapiResourceType, DomainResourceType);

    TDomainResourceEx domainResourceEx = new();

    // set the domain resource property
    PropertyInfo domainResourcePropertyInfo = DomainResourceExType
      .GetProperties(BindingFlags.Public | BindingFlags.Instance)
      .Single(property => property.PropertyType.IsAssignableFrom(DomainResourceType));
    domainResourcePropertyInfo.SetValue(domainResourceEx, domainResource);

    // launch tasks to retrieve swapi subresources
    IEnumerable<(Type SwapiSubresourceType, Type DomainSubresourceType, IEnumerable<Task> SubresourcesTasks)> subresources = GetSubResources()
      .Select(subresourceExpression => subresourceExpression.Invoke(swapiResource))
      .Select(subresource =>
      {
        (Type swapiSubresourceType, Type domainSubresourceType, string[] swapiSubresourceUris) = subresource;
        IEnumerable<int> subresourceIds = swapiSubresourceUris.Select(SwUriUtils.GetIdFromSwapiUri);

        // ReSharper disable once AccessToDisposedClosure
        // we enumerate, so the asyncServiceScope variable was not disposed yet
        object service =
          asyncServiceScope.ServiceProvider.GetRequiredService(ResourceExServiceTypes.SwapiResourceServiceTypeUnbound.MakeGenericType(swapiSubresourceType));
        MethodInfo getByIdMethodInfo = service.GetType().GetMethod(nameof(ISwapiResourceService<object>.GetByIdAsync))!;

        IEnumerable<Task> subresourceTasks = subresourceIds.Select(subresourceId =>
          {
            Task task = (Task)getByIdMethodInfo.Invoke(service, new object[]
            {
              subresourceId,
              cancellationToken
            })!;

            if (task.Status == TaskStatus.Created)
              task.Start();

            return task;
          })
          // make sure to enumerate in order to start all the tasks for this subresource type
          .ToImmutableArray();

        return (SwapiSubresourceType: swapiSubresourceType, DomainSubresourceType: domainSubresourceType, SubresourcesTasks: subresourceTasks);
      })
      // make sure to enumerate in order to start the tasks for all the subresource types
      .ToImmutableArray();

    foreach ((Type swapiSubresourceType, Type domainSubresourceType, IEnumerable<Task> subresourcesTasks) in subresources)
    {
      Type swapiSubresourceArrayType = swapiSubresourceType.MakeArrayType();
      Type taskOfSwapiSubresourceTypeType = ResourceExServiceTypes.TaskTypeUnbound.MakeGenericType(swapiSubresourceType);

      // cast IEnumerable<Task> to IEnumerable<Task<swapiSubresourceType>>
      object enumerableOfTaskOfSwapiSubresourceType = ResourceExServiceTypes.EnumerableCastMethodInfoUnbound
        .MakeGenericMethod(taskOfSwapiSubresourceTypeType)
        .Invoke(null, new object[] { subresourcesTasks })!;

      object swapiSubresourcesWhenAllPromise = ResourceExServiceTypes.TaskWhenAllMethodInfoUnbound
        .MakeGenericMethod(swapiSubresourceType)
        .Invoke(null, new[] { enumerableOfTaskOfSwapiSubresourceType })!;

      await ((Task)swapiSubresourcesWhenAllPromise).WaitAsync(cancellationToken);

      PropertyInfo taskOfSwapiSubresourceArrayResultPropertyInfo = ResourceExServiceTypes.TaskTypeUnbound
        .MakeGenericType(swapiSubresourceArrayType)
        .GetProperty(nameof(Task<object>.Result))!;

      object[] arrayOfSwapiSubresource = (object[])taskOfSwapiSubresourceArrayResultPropertyInfo.GetValue(swapiSubresourcesWhenAllPromise)!;
      // The runtime type is object[]
      object[] arrayOfObjectDomainSubresources = arrayOfSwapiSubresource
        .Select(swapiSubresource => Mapper.Map(swapiSubresource, swapiSubresourceType, domainSubresourceType))
        .ToArray();

      // Find the property which holds the reference to the IEnumerable<domainResourceType>
      PropertyInfo propertyInfo = DomainResourceExType
        .GetProperties()
        .Single(property => property.PropertyType.IsAssignableFrom(ResourceExServiceTypes.IEnumerableTypeUnbound.MakeGenericType(domainSubresourceType)));

      // Cast object[] to IEnumerable<domainSubresourceType>
      object arrayOfDomainSubresources = typeof(Enumerable)
        .GetMethod(nameof(Enumerable.Cast))!
        .MakeGenericMethod(domainSubresourceType)
        .Invoke(null, new object[] { arrayOfObjectDomainSubresources })!;
      propertyInfo.SetValue(domainResourceEx, arrayOfDomainSubresources);
    }

    return domainResourceEx;
  }

  protected abstract IEnumerable<Func<TSwapiResource, (Type SwapiSubresourceType, Type DomainSubresourceType, string[] SwapiSubresourceUris)>>
    GetSubResources();
}
