﻿using System.Diagnostics.CodeAnalysis;
using AutoMapper;
using StarWarsApi.Swapi.Model.Resources;
using StarWarsApi.WebApi.Model;

namespace StarWarsApi.WebApi.AutoMapper;

[SuppressMessage("ReSharper", "UnusedType.Global")]
public class SwapiProfile : Profile
{
  public SwapiProfile()
  {
    CreateMap<People, PeopleDto>()
      .ConvertUsing(people => PeopleDto.From(people));

    CreateMap<Film, FilmDto>()
      .ConvertUsing(film => FilmDto.From(film));

    CreateMap<Starship, StarshipDto>()
      .ConvertUsing(starship => StarshipDto.From(starship));

    CreateMap<Vehicle, VehicleDto>()
      .ConvertUsing(vehicle => VehicleDto.From(vehicle));

    CreateMap<Species, SpeciesDto>()
      .ConvertUsing(species => SpeciesDto.From(species));

    CreateMap<Planet, PlanetDto>()
      .ConvertUsing(planet => PlanetDto.From(planet));
  }
}
