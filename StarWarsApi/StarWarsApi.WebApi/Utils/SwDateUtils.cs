﻿using System.Text.RegularExpressions;

namespace StarWarsApi.WebApi.Utils;

public static class SwDateUtils
{
  private static readonly Regex YearWithEraRegex = new(@"^(?<year>\d+(?:\.\d+)?)\s?(?<era>BBY|ABY)$");

  private static readonly Regex TimeSpanRegex =
    new(@"(?:(?<years>\d+)\s?years?)?\s?(?:(?<months>\d+)\s?months?)?\s?(?:(?<weeks>\d+)\s?weeks?)?\s?(?:(?<days>\d+)\s?days?)?");

  public static TimeSpan? ParseSpeciesLifeSpan(string speciesLifespan)
  {
    if (SwUtils.IsUnknownOrNA(speciesLifespan))
      return null;
    if (speciesLifespan.ToLowerInvariant() is "indefinite")
      return null;

    return TimeSpan.FromDays(365 * float.Parse(speciesLifespan));
  }

  public static TimeSpan ParseSwapiTimeSpan(string timespan)
  {
    return SwUtils.ParseWithRegex(TimeSpanRegex, timespan, match =>
    {
      string matchedYears = match.Groups["years"].ToString();
      string matchedMonths = match.Groups["months"].ToString();
      string matchedWeeks = match.Groups["weeks"].ToString();
      string matchedDays = match.Groups["days"].ToString();

      // ParseSwapiTimeSpan calls TryParse but does not explicitly check whether the conversion succeeded.
      // Either use the return value in a conditional statement or verify that the call site expects that the out
      // argument will be set to the default value when the conversion fails.
      // Default value for int is 0 which is OK in this case
#pragma warning disable CA1806
      int.TryParse(matchedYears, out int years);
      int.TryParse(matchedMonths, out int months);
      int.TryParse(matchedWeeks, out int weeks);
      int.TryParse(matchedDays, out int days);
#pragma warning restore CA1806

      return TimeSpan.FromDays(days + weeks * 7 + months * 30 + years * 365);
    });
  }

  /// <summary>
  /// </summary>
  /// <param name="yearWithEra"></param>
  /// <returns></returns>
  /// <exception cref="FormatException"></exception>
  public static float? ParseYearWithEra(string yearWithEra)
  {
    return SwUtils.ParseWithRegex<float?>(YearWithEraRegex, yearWithEra, match =>
    {
      string matchedYear = match.Groups["year"].ToString();
      string matchedEra = match.Groups["era"].ToString();

      float year = float.Parse(matchedYear);

      if (matchedEra == "BBY")
      {
        return -year;
      }

      return year;
    }, true);
  }
}
