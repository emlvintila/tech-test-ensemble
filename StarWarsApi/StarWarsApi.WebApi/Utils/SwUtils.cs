﻿using System.Globalization;
using System.Runtime.CompilerServices;
using System.Text.RegularExpressions;

namespace StarWarsApi.WebApi.Utils;

public static class SwUtils
{
  private static readonly Regex MegaLightsRegex = new(@"(?<quantity>\d+(?:\.\d+)?)(?:\s?MGLT)?");

  private static readonly Regex SpeedRegex = new(@"(?<speed>\d+)(?:\s?km)?");

  public static bool IsUnknownOrNA(string s) => s.ToLowerInvariant() is "unknown" or "n/a";

  public static string? UnknownOrNAToDefault(string s) => UnknownOrNAToDefault(s, _ => _);

  public static T? UnknownOrNAToDefault<T>(string s, Func<string, T> parse) => IsUnknownOrNA(s) ? default : parse(s);

  public static float? ParseCargoCapacity(string cargoCapacity)
  {
    if (IsUnknownOrNA(cargoCapacity) || cargoCapacity.ToLowerInvariant() is "none")
      return null;

    return float.Parse(cargoCapacity);
  }

  public static IEnumerable<string>? ParseColors(string colors)
  {
    if (IsUnknownOrNA(colors) || colors.ToLowerInvariant() is "none")
      return null;

    return colors.Split(',').Select(color => color.Trim());
  }

  public static IEnumerable<KeyValuePair<string, float>>? ParseGravity(string gravity)
  {
    if (IsUnknownOrNA(gravity))
      return null;

    IEnumerable<string> gravities = gravity.Split(',').Select(g => g.Trim());

    return gravities.Select(grav =>
    {
      string[] gravityParts = grav.Split(' ', 2, StringSplitOptions.TrimEntries | StringSplitOptions.RemoveEmptyEntries);

      return new KeyValuePair<string, float>(gravityParts[1], float.Parse(gravityParts[0]));
    });
  }

  public static float ParseMegaLights(string megaLights)
  {
    Match match = MegaLightsRegex.Match(megaLights);
    if (!match.Success)
      throw new FormatException($"Incorrect format for parameter \"{nameof(megaLights)}\"");

    string matchedQuantity = match.Groups["quantity"].ToString();

    return float.Parse(matchedQuantity);
  }

  public static (int min, int max) ParseVehicleCrew(string crew)
  {
    const char Separator = '-';
    const NumberStyles CrewNumberStyles = NumberStyles.AllowThousands;

    if (crew.Contains(Separator))
    {
      string[] minMaxTuple = crew.Split(Separator, 2, StringSplitOptions.TrimEntries | StringSplitOptions.RemoveEmptyEntries);
      int min = int.Parse(minMaxTuple.First(), CrewNumberStyles);
      int max = int.Parse(minMaxTuple.Last(), CrewNumberStyles);

      return (min, max);
    }

    int minMax = int.Parse(crew, CrewNumberStyles);

    return (minMax, minMax);
  }

  public static float? ParseSpeed(string speed)
  {
    return ParseWithRegex<float?>(SpeedRegex, speed, match => float.Parse(match.Groups["speed"].ToString()), true);
  }

  public static T? ParseWithRegex<T>(Regex regex, string input, Func<Match, T?> fn, bool testForUnknown = false,
    [CallerArgumentExpression("input")] string callerExpression = null!)
  {
    if (testForUnknown && IsUnknownOrNA(input))
      return default;

    Match match = regex.Match(input);
    if (!match.Success)
      throw new FormatException($"Incorrect format for expression \"{callerExpression}\"");

    return fn(match);
  }
}
