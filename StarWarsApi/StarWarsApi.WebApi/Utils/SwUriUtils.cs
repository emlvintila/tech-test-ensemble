﻿using System.Text.RegularExpressions;

namespace StarWarsApi.WebApi.Utils;

public static class SwUriUtils
{
  private static readonly Regex IdMatchRegex = new(@"/(?<id>\d+)/?$");

  /// <summary>
  /// </summary>
  /// <param name="uri"></param>
  /// <returns></returns>
  /// <exception cref="FormatException"></exception>
  public static int GetIdFromSwapiUri(string uri)
  {
    string absolutePath = new Uri(uri).AbsolutePath;
    Match match = IdMatchRegex.Match(absolutePath);
    if (!match.Success)
    {
      throw new FormatException("Uri was not in the correct format. Expected a number in the last path component.");
    }

    string matchedId = match.Groups["id"].ToString();

    return int.Parse(matchedId);
  }
}
