﻿using System.Globalization;
using StarWarsApi.Swapi.Model.Resources;
using StarWarsApi.WebApi.Utils;

namespace StarWarsApi.WebApi.Model;

[Serializable]
public class VehicleDto : VehicleBaseDto
{
  public VehicleDto(string name, string model, string vehicleClass, IEnumerable<string> manufacturers, float? length, decimal? cost,
    (int min, int max) noOfRequiredCrewMembers, int? passengerCapacity, float? maxAtmosphericSpeed, float? cargoCapacity, TimeSpan consumablesMaxSupplyTime,
    IEnumerable<int> filmIds, IEnumerable<int> pilotIds) :
    base(name, model, manufacturers, cost, length, noOfRequiredCrewMembers, passengerCapacity, maxAtmosphericSpeed, cargoCapacity, consumablesMaxSupplyTime,
      filmIds, pilotIds) =>
    VehicleClass = vehicleClass;

  public string VehicleClass { get; }

  public static VehicleDto From(Vehicle vehicle) =>
    new(vehicle.Name,
      vehicle.Model,
      vehicle.VehicleClass,
      vehicle.Manufacturer.Split(',').Select(manufacturer => manufacturer.Trim()),
      SwUtils.UnknownOrNAToDefault<float?>(vehicle.Length, length => float.Parse(length)),
      SwUtils.UnknownOrNAToDefault<decimal?>(vehicle.CostInCredits, cost => decimal.Parse(cost)),
      SwUtils.ParseVehicleCrew(vehicle.Crew),
      SwUtils.UnknownOrNAToDefault<int?>(vehicle.Passengers, passengers => int.Parse(passengers, NumberStyles.AllowThousands)),
      SwUtils.ParseSpeed(vehicle.MaxAtmospheringSpeed),
      SwUtils.ParseCargoCapacity(vehicle.CargoCapacity),
      SwDateUtils.ParseSwapiTimeSpan(vehicle.Consumables),
      vehicle.Films.Select(SwUriUtils.GetIdFromSwapiUri),
      vehicle.Pilots.Select(SwUriUtils.GetIdFromSwapiUri));
}
