﻿using System.Globalization;
using StarWarsApi.Swapi.Model.Resources;
using StarWarsApi.WebApi.Utils;

namespace StarWarsApi.WebApi.Model;

public class StarshipDto : VehicleBaseDto
{
  public StarshipDto(string name, string model, string starshipClass, IEnumerable<string> manufacturers, decimal? cost, float length,
    (int, int) noOfRequiredCrewMembers, int? passengerCapacity, float? maxAtmosphericSpeed, string hyperDriveRating, float maxMegaLightsPerHour,
    float cargoCapacity, TimeSpan consumablesMaxSupplyTime, IEnumerable<int> filmIds, IEnumerable<int> pilotIds) :
    base(name, model, manufacturers, cost, length, noOfRequiredCrewMembers, passengerCapacity, maxAtmosphericSpeed, cargoCapacity, consumablesMaxSupplyTime,
      filmIds, pilotIds)
  {
    StarshipClass = starshipClass;
    HyperDriveRating = hyperDriveRating;
    MaxMegaLightsPerHour = maxMegaLightsPerHour;
  }

  public string StarshipClass { get; }
  public string HyperDriveRating { get; }
  public float MaxMegaLightsPerHour { get; }

  public static StarshipDto From(Starship starship) =>
    new(starship.Name,
      starship.Model,
      starship.StarshipClass,
      starship.Manufacturer.Split(',').Select(manufacturer => manufacturer.Trim()),
      SwUtils.UnknownOrNAToDefault<decimal?>(starship.CostInCredits, cost => decimal.Parse(cost)),
      float.Parse(starship.Length),
      SwUtils.ParseVehicleCrew(starship.Crew),
      SwUtils.UnknownOrNAToDefault<int?>(starship.Passengers, passengers => int.Parse(passengers, NumberStyles.AllowThousands)),
      SwUtils.ParseSpeed(starship.MaxAtmospheringSpeed),
      starship.HyperdriveRating,
      SwUtils.ParseMegaLights(starship.Mglt),
      float.Parse(starship.CargoCapacity),
      SwDateUtils.ParseSwapiTimeSpan(starship.Consumables),
      starship.Films.Select(SwUriUtils.GetIdFromSwapiUri),
      starship.Pilots.Select(SwUriUtils.GetIdFromSwapiUri));
}
