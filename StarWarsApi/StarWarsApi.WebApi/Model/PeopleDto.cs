﻿using System.Collections.Immutable;
using StarWarsApi.Swapi.Model.Resources;
using StarWarsApi.WebApi.Utils;

namespace StarWarsApi.WebApi.Model;

public class PeopleDto
{
  public PeopleDto(string name, float? birthYear, string? gender, string? eyeColor, string? hairColor, float? height, float? mass, string? skinColor, int
    homeWorldId, IEnumerable<int> filmIds, IEnumerable<int> speciesIds, IEnumerable<int> starshipIds, IEnumerable<int> vehicleIds)
  {
    Name = name;
    BirthYear = birthYear;
    Gender = gender;
    EyeColor = eyeColor;
    HairColor = hairColor;
    Height = height;
    Mass = mass;
    SkinColor = skinColor;
    HomeWorldId = homeWorldId;
    FilmIds = filmIds.ToImmutableSortedSet();
    SpeciesIds = speciesIds.ToImmutableSortedSet();
    StarshipIds = starshipIds.ToImmutableSortedSet();
    VehicleIds = vehicleIds.ToImmutableSortedSet();
  }

  public string Name { get; }
  public float? BirthYear { get; }
  public string? Gender { get; }
  public string? EyeColor { get; }
  public string? HairColor { get; }
  public float? Height { get; }
  public float? Mass { get; }
  public string? SkinColor { get; }
  public int HomeWorldId { get; }
  public IImmutableSet<int> FilmIds { get; }
  public IImmutableSet<int> SpeciesIds { get; }
  public IImmutableSet<int> StarshipIds { get; }
  public IImmutableSet<int> VehicleIds { get; }

  public static PeopleDto From(People people) =>
    new(people.Name,
      SwDateUtils.ParseYearWithEra(people.BirthYear),
      SwUtils.UnknownOrNAToDefault(people.Gender),
      SwUtils.UnknownOrNAToDefault(people.EyeColor),
      SwUtils.UnknownOrNAToDefault(people.HairColor),
      SwUtils.UnknownOrNAToDefault<float?>(people.Height, height => float.Parse(height)),
      SwUtils.UnknownOrNAToDefault<float?>(people.Mass, mass => float.Parse(mass)),
      SwUtils.UnknownOrNAToDefault(people.SkinColor),
      SwUriUtils.GetIdFromSwapiUri(people.HomeWorld),
      people.Films.Select(SwUriUtils.GetIdFromSwapiUri),
      people.Species.Select(SwUriUtils.GetIdFromSwapiUri),
      people.Starships.Select(SwUriUtils.GetIdFromSwapiUri),
      people.Vehicles.Select(SwUriUtils.GetIdFromSwapiUri));
}
