﻿namespace StarWarsApi.WebApi.Model;

[Serializable]
public abstract class VehicleBaseDto
{
  protected VehicleBaseDto(string name, string model, IEnumerable<string> manufacturers, decimal? cost, float? length, (int, int) noOfRequiredCrewMembers,
    int? passengerCapacity, float? maxAtmosphericSpeed, float? cargoCapacity, TimeSpan consumablesMaxSupplyTime, IEnumerable<int> filmIds,
    IEnumerable<int> pilotIds)
  {
    Name = name;
    Model = model;
    Manufacturers = manufacturers;
    Cost = cost;
    Length = length;
    (MinNoOfRequiredCrewMembers, MaxNoOfRequiredCrewMembers) = noOfRequiredCrewMembers;
    PassengerCapacity = passengerCapacity;
    MaxAtmosphericSpeed = maxAtmosphericSpeed;
    CargoCapacity = cargoCapacity;
    ConsumablesMaxSupplyTime = consumablesMaxSupplyTime;
    FilmIds = filmIds;
    PilotIds = pilotIds;
  }

  public string Name { get; }
  public string Model { get; }
  public IEnumerable<string> Manufacturers { get; }
  public decimal? Cost { get; }
  public float? Length { get; }
  public int MinNoOfRequiredCrewMembers { get; }
  public int MaxNoOfRequiredCrewMembers { get; }
  public int? PassengerCapacity { get; }
  public float? MaxAtmosphericSpeed { get; }
  public float? CargoCapacity { get; }
  public TimeSpan ConsumablesMaxSupplyTime { get; }
  public IEnumerable<int> FilmIds { get; }
  public IEnumerable<int> PilotIds { get; }
}
