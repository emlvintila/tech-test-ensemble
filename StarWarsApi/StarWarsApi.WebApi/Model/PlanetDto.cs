﻿using System.Collections.Immutable;
using System.Globalization;
using StarWarsApi.Swapi.Model.Resources;
using StarWarsApi.WebApi.Utils;

namespace StarWarsApi.WebApi.Model;

public class PlanetDto
{
  public PlanetDto(string name, float diameter, TimeSpan? rotationPeriod, TimeSpan? orbitalPeriod, IDictionary<string, float>? gravity,
    long? population, IEnumerable<string> climates, IEnumerable<string> terrains, float? surfaceWaterPercentage, IEnumerable<int> residentIds,
    IEnumerable<int> filmIds)
  {
    Name = name;
    Diameter = diameter;
    RotationPeriod = rotationPeriod;
    OrbitalPeriod = orbitalPeriod;
    Gravity = gravity?.ToImmutableDictionary();
    Population = population;
    Climates = climates.ToImmutableArray();
    Terrains = terrains.ToImmutableArray();
    SurfaceWaterPercentage = surfaceWaterPercentage;
    ResidentIds = residentIds.ToImmutableArray();
    FilmIds = filmIds.ToImmutableArray();
  }

  public string Name { get; }
  public float Diameter { get; }
  public TimeSpan? RotationPeriod { get; }
  public TimeSpan? OrbitalPeriod { get; }
  public IImmutableDictionary<string, float>? Gravity { get; }
  public long? Population { get; }
  public IEnumerable<string> Climates { get; }
  public IEnumerable<string> Terrains { get; }
  public float? SurfaceWaterPercentage { get; }
  public IEnumerable<int> ResidentIds { get; }
  public IEnumerable<int> FilmIds { get; }

  public static PlanetDto From(Planet planet) =>
    new(planet.Name,
      float.Parse(planet.Diameter),
      SwUtils.UnknownOrNAToDefault(planet.RotationPeriod, rotationPeriod => TimeSpan.FromDays(float.Parse(rotationPeriod))),
      SwUtils.UnknownOrNAToDefault(planet.OrbitalPeriod, orbitalPeriod => TimeSpan.FromDays(float.Parse(orbitalPeriod))),
      SwUtils.ParseGravity(planet.Gravity)?.ToImmutableDictionary(),
      SwUtils.UnknownOrNAToDefault<long?>(planet.Population, population => long.Parse(population, NumberStyles.AllowThousands)),
      planet.Climate.Split(',').Select(climate => climate.Trim()),
      planet.Terrain.Split(',').Select(terrain => terrain.Trim()),
      SwUtils.UnknownOrNAToDefault<float?>(planet.SurfaceWater, surfaceWater => float.Parse(surfaceWater) / 100f),
      planet.Residents.Select(SwUriUtils.GetIdFromSwapiUri),
      planet.Films.Select(SwUriUtils.GetIdFromSwapiUri));
}
