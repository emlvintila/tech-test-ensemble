﻿using System.Collections.Immutable;
using StarWarsApi.Swapi.Model.Resources;
using StarWarsApi.WebApi.Utils;

namespace StarWarsApi.WebApi.Model;

public class FilmDto
{
  public FilmDto(string title, int episodeId, string openingCrawl, string director, IEnumerable<string> producers, DateTime releaseDate, IEnumerable<int>
    speciesIds, IEnumerable<int> starshipIds, IEnumerable<int> vehicleIds, IEnumerable<int> characterIds, IEnumerable<int> planetIds)
  {
    Title = title;
    EpisodeId = episodeId;
    OpeningCrawl = openingCrawl;
    Director = director;
    Producers = producers.ToImmutableArray();
    ReleaseDate = releaseDate;
    SpeciesIds = speciesIds.ToImmutableArray();
    StarshipIds = starshipIds.ToImmutableArray();
    VehicleIds = vehicleIds.ToImmutableArray();
    CharacterIds = characterIds.ToImmutableArray();
    PlanetIds = planetIds.ToImmutableArray();
  }

  public string Title { get; }
  public int EpisodeId { get; }
  public string OpeningCrawl { get; }
  public string Director { get; }
  public IEnumerable<string> Producers { get; }
  public DateTime ReleaseDate { get; }
  public IEnumerable<int> SpeciesIds { get; }
  public IEnumerable<int> StarshipIds { get; }
  public IEnumerable<int> VehicleIds { get; }
  public IEnumerable<int> CharacterIds { get; }
  public IEnumerable<int> PlanetIds { get; }

  public static FilmDto From(Film film) =>
    new(film.Title,
      film.EpisodeId,
      film.OpeningCrawl,
      film.Director,
      film.Producer.Split(',').Select(producer => producer.Trim()),
      DateTime.Parse(film.ReleaseDate),
      film.Species.Select(SwUriUtils.GetIdFromSwapiUri),
      film.Starships.Select(SwUriUtils.GetIdFromSwapiUri),
      film.Vehicles.Select(SwUriUtils.GetIdFromSwapiUri),
      film.Characters.Select(SwUriUtils.GetIdFromSwapiUri),
      film.Planets.Select(SwUriUtils.GetIdFromSwapiUri));
}
