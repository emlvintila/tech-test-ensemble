﻿using System.Collections.Immutable;
using StarWarsApi.Swapi.Model.Resources;
using StarWarsApi.WebApi.Utils;

namespace StarWarsApi.WebApi.Model;

[Serializable]
public class SpeciesDto
{
  public SpeciesDto(string name, string classification, string designation, float? averageHeight, TimeSpan? averageLifeSpan, IEnumerable<string>? eyeColors,
    IEnumerable<string>? hairColors, IEnumerable<string>? skinColors, string? language, int? homeworldId, IEnumerable<int> peopleIds, IEnumerable<int> filmIds)
  {
    Name = name;
    Classification = classification;
    Designation = designation;
    AverageHeight = averageHeight;
    AverageLifeSpan = averageLifeSpan;
    EyeColors = eyeColors?.ToImmutableArray();
    HairColors = hairColors?.ToImmutableArray();
    SkinColors = skinColors?.ToImmutableArray();
    Language = language;
    HomeworldId = homeworldId;
    PeopleIds = peopleIds.ToImmutableArray();
    FilmIds = filmIds.ToImmutableArray();
  }

  public string Name { get; }
  public string Classification { get; }
  public string Designation { get; }
  public float? AverageHeight { get; }
  public TimeSpan? AverageLifeSpan { get; }
  public IEnumerable<string>? EyeColors { get; }
  public IEnumerable<string>? HairColors { get; }
  public IEnumerable<string>? SkinColors { get; }
  public string? Language { get; }
  public int? HomeworldId { get; }
  public IEnumerable<int> PeopleIds { get; }
  public IEnumerable<int> FilmIds { get; }

  public static SpeciesDto From(Species species) =>
    new(species.Name,
      species.Classification,
      species.Designation,
      SwUtils.UnknownOrNAToDefault<float?>(species.AverageHeight, averageHeight => float.Parse(averageHeight)),
      SwDateUtils.ParseSpeciesLifeSpan(species.AverageLifespan),
      SwUtils.ParseColors(species.EyeColors),
      SwUtils.ParseColors(species.HairColors),
      SwUtils.ParseColors(species.SkinColors),
      SwUtils.UnknownOrNAToDefault(species.Language),
      species.Homeworld is not null ? SwUtils.UnknownOrNAToDefault(species.Homeworld, SwUriUtils.GetIdFromSwapiUri) : null,
      species.People.Select(SwUriUtils.GetIdFromSwapiUri),
      species.Films.Select(SwUriUtils.GetIdFromSwapiUri));
}
