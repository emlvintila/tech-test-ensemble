﻿using System.Collections.Immutable;
using System.Diagnostics.CodeAnalysis;

namespace StarWarsApi.WebApi.Model;

[Serializable]
[SuppressMessage("ReSharper", "UnusedMember.Global", Justification = "Used with reflection by FilmsExService")]
public class FilmExDto
{
  private ImmutableArray<PeopleDto> characters;
  private ImmutableArray<PlanetDto> planets;
  private ImmutableArray<SpeciesDto> species;
  private ImmutableArray<StarshipDto> starships;
  private ImmutableArray<VehicleDto> vehicles;

  public FilmDto Film { get; set; } = null!;

  public IEnumerable<PeopleDto> Characters
  {
    get => characters;
    set => characters = value.ToImmutableArray();
  }

  public IEnumerable<PlanetDto> Planets
  {
    get => planets;
    set => planets = value.ToImmutableArray();
  }

  public IEnumerable<SpeciesDto> Species
  {
    get => species;
    set => species = value.ToImmutableArray();
  }

  public IEnumerable<StarshipDto> Starships
  {
    get => starships;
    set => starships = value.ToImmutableArray();
  }

  public IEnumerable<VehicleDto> Vehicles
  {
    get => vehicles;
    set => vehicles = value.ToImmutableArray();
  }
}
