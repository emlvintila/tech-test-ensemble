﻿using StarWarsApi.WebApi.Model;

namespace StarWarsApi.WebApi.IntegrationTesting.Controllers.VehiclesController;

public class VehiclesControllerIntegrationTest : ControllerIntegrationTestBase<VehicleDto>
{
  protected override string GetGetPath() => "/api/vehicles/";

  protected override int GetByIdId() => 4;
}
