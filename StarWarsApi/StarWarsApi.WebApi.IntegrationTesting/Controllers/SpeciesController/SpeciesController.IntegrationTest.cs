﻿using StarWarsApi.WebApi.Model;

namespace StarWarsApi.WebApi.IntegrationTesting.Controllers.SpeciesController;

public class SpeciesControllerIntegrationTest : ControllerIntegrationTestBase<SpeciesDto>
{
  protected override string GetGetPath() => "/api/species/";
}
