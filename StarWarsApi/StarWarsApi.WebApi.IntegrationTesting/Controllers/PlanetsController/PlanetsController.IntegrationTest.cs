﻿using StarWarsApi.WebApi.Model;

namespace StarWarsApi.WebApi.IntegrationTesting.Controllers.PlanetsController;

public class PlanetsControllerIntegrationTest : ControllerIntegrationTestBase<PlanetDto>
{
  protected override string GetGetPath() => "/api/planets/";
}
