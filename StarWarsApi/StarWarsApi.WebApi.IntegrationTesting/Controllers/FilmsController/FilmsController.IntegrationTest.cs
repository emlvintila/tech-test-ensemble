﻿using StarWarsApi.WebApi.Model;

namespace StarWarsApi.WebApi.IntegrationTesting.Controllers.FilmsController;

public class FilmsControllerIntegrationTest : ControllerIntegrationTestBase<FilmDto>
{
  protected override string GetGetPath() => "/api/films/";
}
