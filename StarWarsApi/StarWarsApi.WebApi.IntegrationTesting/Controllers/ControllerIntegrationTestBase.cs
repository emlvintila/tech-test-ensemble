﻿using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Testing;
using NUnit.Framework;
using StarWarsApi.Swapi;

namespace StarWarsApi.WebApi.IntegrationTesting.Controllers;

[TestFixture]
public abstract class ControllerIntegrationTestBase<T>
{
  [SetUp]
  public virtual void Setup()
  {
    applicationFactory = new WebApplicationFactory<Program>().WithWebHostBuilder(_ => { });

    HttpClient = applicationFactory.CreateClient();
  }

  private WebApplicationFactory<Program> applicationFactory = null!;
  protected HttpClient HttpClient { get; private set; } = null!;

  [Test]
  public async Task TestGet()
  {
    T[] ts = await HttpClient.GetAndDeserializeAsync<T[]>(GetGetPath());

    AssertGet(ts);
  }

  [Test]
  public async Task TestGetById()
  {
    T t = await HttpClient.GetAndDeserializeAsync<T>($"{GetByIdPath()}{GetByIdId()}");

    AssertGetById(t);
  }

  protected virtual void AssertGet(T[] ts) => Assert.Positive(ts.Length);

  protected virtual void AssertGetById(T t) => Assert.IsNotNull(t);

  protected abstract string GetGetPath();

  protected virtual string GetByIdPath() => GetGetPath();

  protected virtual int GetByIdId() => 1;
}
