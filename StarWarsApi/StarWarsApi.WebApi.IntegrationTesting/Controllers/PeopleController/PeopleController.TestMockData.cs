﻿using System;
using StarWarsApi.Swapi.Model;
using StarWarsApi.Swapi.Model.Resources;

namespace StarWarsApi.WebApi.IntegrationTesting.Controllers.PeopleController;

public partial class PeopleControllerTest
{
  private static readonly People MockPeopleLuke = new("Luke SkyTester",
    "19BBY",
    "blue",
    "male",
    "blond",
    "172",
    "77",
    "fair",
    "https://swapi.test/api/planets/1/",
    new[]
    {
      "https://swapi.test/api/films/1/",
      "https://swapi.test/api/films/2/",
      "https://swapi.test/api/films/3/",
      "https://swapi.test/api/films/6/"
    },
    Array.Empty<string>(),
    new[]
    {
      "https://swapi.test/api/starships/12/",
      "https://swapi.test/api/starships/22/"
    },
    new[]
    {
      "https://swapi.test/api/vehicles/14/",
      "https://swapi.test/api/vehicles/30/"
    },
    "https://swapi.test/api/people/1/",
    "2014-12-09T13:50:51.644000Z",
    "2014-12-20T21:17:56.891000Z");

  private static readonly People MockPeopleC3P0 = new("C-3PO Test",
    "112BBY",
    "yellow",
    "n/a",
    "n/a",
    "167",
    "75",
    "gold",
    "https://swapi.test/api/planets/1/",
    new[]
    {
      "https://swapi.test/api/films/1/",
      "https://swapi.test/api/films/2/",
      "https://swapi.test/api/films/3/",
      "https://swapi.test/api/films/4/",
      "https://swapi.test/api/films/5/",
      "https://swapi.test/api/films/6/"
    },
    new[] { "https://swapi.test/api/species/2/" },
    Array.Empty<string>(),
    Array.Empty<string>(),
    "https://swapi.test/api/people/2/",
    "2014-12-10T15:10:51.357000Z",
    "2014-12-20T21:17:50.309000Z");

  private static readonly People[] MockPeoples =
  {
    MockPeopleLuke,
    MockPeopleC3P0
  };

  private static readonly ISwapiResultsList<People> MockPeopleList =
    new SwapiResultsList<People>(MockPeoples.Length, null, null, MockPeoples);
}
