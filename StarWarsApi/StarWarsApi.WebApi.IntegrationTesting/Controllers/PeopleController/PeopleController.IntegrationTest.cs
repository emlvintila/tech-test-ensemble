﻿using System;
using NUnit.Framework;
using StarWarsApi.WebApi.Model;

namespace StarWarsApi.WebApi.IntegrationTesting.Controllers.PeopleController;

public class PeopleControllerIntegrationTest : ControllerIntegrationTestBase<PeopleDto>
{
  protected override void AssertGetById(PeopleDto people)
  {
    Assert.AreEqual("C-3PO", people!.Name);
    Assert.AreEqual(-112, people.BirthYear);
    Assert.IsNull(people.Gender);
    Assert.AreEqual("yellow", people.EyeColor);
    Assert.IsNull(people.HairColor);
    Assert.AreEqual(167, people.Height);
    Assert.AreEqual(75, people.Mass);
    Assert.AreEqual("gold", people.SkinColor);
    Assert.AreEqual(1, people.HomeWorldId);
    // @formatter:off
    Assert.AreEqual(new[] { 1, 2, 3, 4, 5, 6 }, people.FilmIds);
    Assert.AreEqual(new[] { 2 }, people.SpeciesIds);
    Assert.AreEqual(Array.Empty<int>(), people.StarshipIds);
    Assert.AreEqual(Array.Empty<int>(), people.VehicleIds);
    // @formatter:on
  }

  protected override string GetGetPath() => "/api/people/";

  protected override int GetByIdId() => 2;
}
