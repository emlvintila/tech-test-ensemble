using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.AspNetCore.TestHost;
using Microsoft.Extensions.DependencyInjection;
using Moq;
using NUnit.Framework;
using StarWarsApi.Swapi;
using StarWarsApi.Swapi.Model.Resources;
using StarWarsApi.Swapi.Services;
using StarWarsApi.WebApi.Model;

namespace StarWarsApi.WebApi.IntegrationTesting.Controllers.PeopleController;

public partial class PeopleControllerTest
{
  [Test]
  public async Task TestGetPeople()
  {
    Mock<ISwapiResourceService<People>> mockSwapiService = new();
    mockSwapiService
      .Setup(swapiService => swapiService.GetPageAsync(It.IsAny<int>(), It.IsAny<string>(), It.IsAny<CancellationToken>()))
      .Returns(Task.FromResult(MockPeopleList));

    WebApplicationFactory<Program> applicationFactory = new WebApplicationFactory<Program>()
      .WithWebHostBuilder(builder => { builder.ConfigureTestServices(services => { services.AddScoped(_ => mockSwapiService.Object); }); });

    HttpClient httpClient = applicationFactory.CreateClient();

    PeopleDto[] peoples = await httpClient.GetAndDeserializeAsync<PeopleDto[]>("/api/people/");

    Assert.Positive(peoples.Length);
    Assert.That(() => peoples, Is.All.Property(nameof(PeopleDto.Name)).Not.Empty);
    Assert.That(() => peoples, Is.All.Property(nameof(PeopleDto.HomeWorldId)).Positive);
  }

  [Test]
  public async Task TestGetPeopleReturns503WithErrorMessage()
  {
    Mock<ISwapiResourceService<People>> mockSwapiService = new();
    mockSwapiService
      .Setup(swapiService => swapiService.GetPageAsync(It.IsAny<int>(), It.IsAny<string>(), It.IsAny<CancellationToken>()))
      .Throws<ServiceUnavailableException>();
    WebApplicationFactory<Program> applicationFactory = new WebApplicationFactory<Program>()
      .WithWebHostBuilder(builder => { builder.ConfigureTestServices(services => { services.AddScoped(_ => mockSwapiService.Object); }); });

    HttpClient httpClient = applicationFactory.CreateClient();
    HttpResponseMessage response = await httpClient.GetAsync("/api/people/");

    Assert.AreEqual(response.StatusCode, HttpStatusCode.ServiceUnavailable);

    string responseBody = await response.Content.ReadAsStringAsync();
    Assert.That(() => responseBody, Is.SupersetOf("An unexpected problem occured with the upstream service"));
  }
}
