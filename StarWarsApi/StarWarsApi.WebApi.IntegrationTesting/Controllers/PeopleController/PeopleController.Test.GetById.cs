﻿using System;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.AspNetCore.TestHost;
using Microsoft.Extensions.DependencyInjection;
using Moq;
using NUnit.Framework;
using StarWarsApi.Swapi;
using StarWarsApi.Swapi.Model.Resources;
using StarWarsApi.Swapi.Services;
using StarWarsApi.WebApi.Model;

namespace StarWarsApi.WebApi.IntegrationTesting.Controllers.PeopleController;

public partial class PeopleControllerTest
{
  [Test]
  public async Task TestGetPeopleById()
  {
    Mock<ISwapiResourceService<People>> mockSwapiService = new();
    mockSwapiService
      .Setup(swapiService => swapiService.GetByIdAsync(1, It.IsAny<CancellationToken>()))
      .Returns(Task.FromResult(MockPeopleLuke)!);
    mockSwapiService
      .Setup(swapiService => swapiService.GetByIdAsync(2, It.IsAny<CancellationToken>()))
      .Returns(Task.FromResult(MockPeopleC3P0)!);
    WebApplicationFactory<Program> applicationFactory = new WebApplicationFactory<Program>()
      .WithWebHostBuilder(builder => { builder.ConfigureTestServices(services => { services.AddScoped(_ => mockSwapiService.Object); }); });

    HttpClient httpClient = applicationFactory.CreateClient();

    // ReSharper disable once InconsistentNaming
    PeopleDto c3po = await httpClient.GetAndDeserializeAsync<PeopleDto>("/api/people/2");

    Assert.AreEqual("C-3PO Test", c3po.Name);
    Assert.AreEqual(-112, c3po.BirthYear);
    Assert.IsNull(c3po.Gender);
    Assert.AreEqual("yellow", c3po.EyeColor);
    Assert.IsNull(c3po.HairColor);
    Assert.AreEqual(167, c3po.Height);
    Assert.AreEqual(75, c3po.Mass);
    Assert.AreEqual("gold", c3po.SkinColor);
    Assert.AreEqual(1, c3po.HomeWorldId);
    // @formatter:off
    Assert.AreEqual(new[]{1, 2, 3, 4, 5, 6}, c3po.FilmIds);
    Assert.AreEqual(new[]{2}, c3po.SpeciesIds);
    Assert.AreEqual(Array.Empty<int>(), c3po.StarshipIds);
    Assert.AreEqual(Array.Empty<int>(), c3po.VehicleIds);
    // @formatter:on
  }

  [Test]
  public async Task TestGetPeopleByIdReturns503WithErrorMessage()
  {
    Mock<ISwapiResourceService<People>> mockSwapiService = new();
    mockSwapiService
      .Setup(swapiService => swapiService.GetByIdAsync(It.IsAny<int>(), It.IsAny<CancellationToken>()))
      .Throws<ServiceUnavailableException>();
    WebApplicationFactory<Program> applicationFactory = new WebApplicationFactory<Program>()
      .WithWebHostBuilder(builder => { builder.ConfigureTestServices(services => { services.AddScoped(_ => mockSwapiService.Object); }); });

    HttpClient httpClient = applicationFactory.CreateClient();
    HttpResponseMessage response = await httpClient.GetAsync("/api/people/1");

    Assert.AreEqual(response.StatusCode, HttpStatusCode.ServiceUnavailable);

    string responseBody = await response.Content.ReadAsStringAsync();
    Assert.That(() => responseBody, Is.SupersetOf("An unexpected problem occured with the upstream service"));
  }

  [Test]
  public async Task TestGetPeopleByIdReturns404ForNotFoundId()
  {
    Mock<ISwapiResourceService<People>> mockSwapiService = new();
    mockSwapiService
      .Setup(swapiService => swapiService.GetByIdAsync(It.IsAny<int>(), It.IsAny<CancellationToken>()))
      .Returns(Task.FromResult<People?>(null));
    WebApplicationFactory<Program> applicationFactory = new WebApplicationFactory<Program>()
      .WithWebHostBuilder(builder => { builder.ConfigureTestServices(services => { services.AddScoped(_ => mockSwapiService.Object); }); });

    HttpClient httpClient = applicationFactory.CreateClient();

    HttpResponseMessage response = await httpClient.GetAsync("/api/people/1");
    Assert.AreEqual(HttpStatusCode.NotFound, response.StatusCode);
  }
}
