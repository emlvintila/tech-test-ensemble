﻿using StarWarsApi.WebApi.Model;

namespace StarWarsApi.WebApi.IntegrationTesting.Controllers.StarshipsController;

public class StarshipsControllerIntegrationTest : ControllerIntegrationTestBase<StarshipDto>
{
  protected override string GetGetPath() => "/api/starships/";

  protected override int GetByIdId() => 2;
}
