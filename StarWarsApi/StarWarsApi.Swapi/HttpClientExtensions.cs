﻿using System.Text;
using Newtonsoft.Json;

namespace StarWarsApi.Swapi;

public static class HttpClientExtensions
{
  public static async Task<T> GetAndDeserializeAsync<T>(this HttpClient httpClient, string uri, CancellationToken cancellationToken = default) =>
    await GetAndDeserializeAsync<T>(httpClient, uri, JsonSerializer.Create(), cancellationToken).ConfigureAwait(false);

  public static async Task<T> GetAndDeserializeAsync<T>(this HttpClient httpClient, string uri, JsonSerializer jsonSerializer,
    CancellationToken cancellationToken = default)
  {
    HttpResponseMessage httpResponseMessage = await httpClient.GetAsync(uri, cancellationToken).ConfigureAwait(false);
    httpResponseMessage.EnsureSuccessStatusCode();

    await using Stream httpContentStream = await httpResponseMessage.Content.ReadAsStreamAsync(cancellationToken);
    using StreamReader streamReader = new(httpContentStream, Encoding.UTF8);

    return (T)jsonSerializer.Deserialize(streamReader, typeof(T))!;
  }
}
