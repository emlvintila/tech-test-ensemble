﻿namespace StarWarsApi.Swapi.Model;

[Serializable]
public abstract class VehicleBase : SwapiResourceBase
{
  // ReSharper disable InconsistentNaming,IdentifierTypo
  protected VehicleBase(string name, string model, string manufacturer, string length, string cost_in_credits, string crew, string passengers,
    string max_atmosphering_speed, string cargo_capacity, string consumables, string[] films, string[] pilots, string url, string created, string edited) :
    base(url, created, edited)
  // ReSharper restore InconsistentNaming,IdentifierTypo
  {
    Name = name;
    Model = model;
    Manufacturer = manufacturer;
    Length = length;
    CostInCredits = cost_in_credits;
    Crew = crew;
    Passengers = passengers;
    MaxAtmospheringSpeed = max_atmosphering_speed;
    CargoCapacity = cargo_capacity;
    Consumables = consumables;
  }

  public string Name { get; }
  public string Model { get; }
  public string Manufacturer { get; }
  public string Length { get; }
  public string CostInCredits { get; }
  public string Crew { get; }
  public string Passengers { get; }

  // ReSharper disable once IdentifierTypo
  public string MaxAtmospheringSpeed { get; }
  public string CargoCapacity { get; }
  public string Consumables { get; }
}
