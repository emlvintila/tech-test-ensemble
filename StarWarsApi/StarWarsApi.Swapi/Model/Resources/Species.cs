﻿using Newtonsoft.Json;

namespace StarWarsApi.Swapi.Model.Resources;

[Serializable]
public class Species : SwapiResourceBase
{
  // ReSharper disable InconsistentNaming
  [JsonConstructor]
  public Species(string name, string classification, string designation, string average_height, string average_lifespan, string eye_colors, string hair_colors,
    string skin_colors, string language, string? homeworld, string[] people, string[] films, string url, string created, string edited) :
    base(url, created, edited)
  // ReSharper restore InconsistentNaming
  {
    Name = name;
    Classification = classification;
    Designation = designation;
    AverageHeight = average_height;
    AverageLifespan = average_lifespan;
    EyeColors = eye_colors;
    HairColors = hair_colors;
    SkinColors = skin_colors;
    Language = language;
    Homeworld = homeworld;
    People = people;
    Films = films;
  }

  public string Name { get; }
  public string Classification { get; }
  public string Designation { get; }
  public string AverageHeight { get; }
  public string AverageLifespan { get; }
  public string EyeColors { get; }
  public string HairColors { get; }
  public string SkinColors { get; }
  public string Language { get; }
  public string? Homeworld { get; }
  public string[] People { get; }
  public string[] Films { get; }
}
