﻿using Newtonsoft.Json;

namespace StarWarsApi.Swapi.Model.Resources;

[Serializable]
public class Starship : VehicleBase
{
  // ReSharper disable InconsistentNaming,IdentifierTypo
  [JsonConstructor]
  public Starship(string name, string model, string starship_class, string manufacturer, string cost_in_credits, string length, string crew, string
    passengers, string max_atmosphering_speed, string hyperdrive_rating, string MGLT, string cargo_capacity, string consumables, string[] films, string[]
    pilots, string url, string created, string edited) :
    base(name, model, manufacturer, length, cost_in_credits, crew, passengers, max_atmosphering_speed, cargo_capacity, consumables, films, pilots, url, created,
      edited)
  // ReSharper restore InconsistentNaming,IdentifierTypo
  {
    StarshipClass = starship_class;
    HyperdriveRating = hyperdrive_rating;
    Mglt = MGLT;
    Films = films;
    Pilots = pilots;
  }

  public string StarshipClass { get; }
  public string[] Films { get; }
  public string[] Pilots { get; }

  // ReSharper disable IdentifierTypo
  public string HyperdriveRating { get; }

  public string Mglt { get; }
  // ReSharper restore IdentifierTypo
}
