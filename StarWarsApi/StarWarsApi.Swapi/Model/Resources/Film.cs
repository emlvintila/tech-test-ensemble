﻿using Newtonsoft.Json;

namespace StarWarsApi.Swapi.Model.Resources;

[Serializable]
public class Film : SwapiResourceBase
{
  // ReSharper disable InconsistentNaming
  [JsonConstructor]
  public Film(string title, int episode_id, string opening_crawl, string director, string producer, string release_date, string[] species, string[]
    starships, string[] vehicles, string[] characters, string[] planets, string url, string created, string edited) :
    base(url, created, edited)
  // ReSharper restore InconsistentNaming
  {
    Title = title;
    EpisodeId = episode_id;
    OpeningCrawl = opening_crawl;
    Director = director;
    Producer = producer;
    ReleaseDate = release_date;
    Species = species;
    Starships = starships;
    Vehicles = vehicles;
    Characters = characters;
    Planets = planets;
  }

  public string Title { get; }
  public int EpisodeId { get; }
  public string OpeningCrawl { get; }
  public string Director { get; }
  public string Producer { get; }
  public string ReleaseDate { get; }
  public string[] Species { get; }
  public string[] Starships { get; }
  public string[] Vehicles { get; }
  public string[] Characters { get; }
  public string[] Planets { get; }
}
