﻿using Newtonsoft.Json;

namespace StarWarsApi.Swapi.Model.Resources;

[Serializable]
public class Vehicle : VehicleBase
{
  // ReSharper disable InconsistentNaming,IdentifierTypo
  [JsonConstructor]
  public Vehicle(string name, string model, string vehicle_class, string manufacturer, string length, string cost_in_credits, string crew, string passengers,
    string max_atmosphering_speed, string cargo_capacity, string consumables, string[] films, string[] pilots, string url, string created,
    string edited) : base(name, model, manufacturer, length, cost_in_credits, crew, passengers, max_atmosphering_speed, cargo_capacity, consumables, films,
    pilots, url, created, edited)
  // ReSharper restore InconsistentNaming,IdentifierTypo
  {
    VehicleClass = vehicle_class;
    Films = films;
    Pilots = pilots;
  }

  public string VehicleClass { get; }
  public string[] Films { get; }
  public string[] Pilots { get; }
}
