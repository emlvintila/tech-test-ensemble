﻿using Newtonsoft.Json;

namespace StarWarsApi.Swapi.Model.Resources;

[Serializable]
public class People : SwapiResourceBase
{
  // ReSharper disable InconsistentNaming
  [JsonConstructor]
  public People(string name, string birth_year, string eye_color, string gender, string hair_color, string height, string mass, string skin_color,
    string homeworld, string[] films, string[] species, string[] starships, string[] vehicles, string url, string created, string edited) :
    base(url, created, edited)
  // ReSharper restore InconsistentNaming
  {
    Name = name;
    BirthYear = birth_year;
    EyeColor = eye_color;
    Gender = gender;
    HairColor = hair_color;
    Height = height;
    Mass = mass;
    SkinColor = skin_color;
    HomeWorld = homeworld;
    Films = films;
    Species = species;
    Starships = starships;
    Vehicles = vehicles;
  }

  public string Name { get; }
  public string BirthYear { get; }
  public string Gender { get; }
  public string EyeColor { get; }
  public string HairColor { get; }
  public string Height { get; }
  public string Mass { get; }
  public string SkinColor { get; }
  public string HomeWorld { get; }
  public string[] Films { get; }
  public string[] Species { get; }
  public string[] Starships { get; }
  public string[] Vehicles { get; }
}
