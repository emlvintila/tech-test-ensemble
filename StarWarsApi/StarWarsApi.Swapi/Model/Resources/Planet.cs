﻿using Newtonsoft.Json;

namespace StarWarsApi.Swapi.Model.Resources;

[Serializable]
public class Planet : SwapiResourceBase
{
  // ReSharper disable InconsistentNaming
  [JsonConstructor]
  public Planet(string name, string diameter, string rotation_period, string orbital_period, string gravity, string population, string climate, string terrain,
    string surface_water, string[] residents, string[] films, string url, string created, string edited) :
    base(url, created, edited)
  // ReSharper restore InconsistentNaming
  {
    Name = name;
    Diameter = diameter;
    RotationPeriod = rotation_period;
    OrbitalPeriod = orbital_period;
    Gravity = gravity;
    Population = population;
    Climate = climate;
    Terrain = terrain;
    SurfaceWater = surface_water;
    Residents = residents;
    Films = films;
  }

  public string Name { get; }
  public string Diameter { get; }
  public string RotationPeriod { get; }
  public string OrbitalPeriod { get; }
  public string Gravity { get; }
  public string Population { get; }
  public string Climate { get; }
  public string Terrain { get; }
  public string SurfaceWater { get; }
  public string[] Residents { get; }
  public string[] Films { get; }
}
