﻿using System.Collections.Immutable;
using Newtonsoft.Json;

namespace StarWarsApi.Swapi.Model;

public class SwapiResultsList<T> : ISwapiResultsList<T>
{
  [JsonConstructor]
  public SwapiResultsList(int count, string? next, string? previous, IEnumerable<T> results)
  {
    Count = count;
    Next = next;
    Previous = previous;
    Results = results.ToImmutableArray();
  }

  public int Count { get; }
  public string? Next { get; }
  public string? Previous { get; }
  public IEnumerable<T> Results { get; }
}
