﻿namespace StarWarsApi.Swapi.Model;

public interface ISwapiResultsList<out T>
{
  /// <summary>
  ///   Total count of People that would've been returned by the call if there was no pagination.
  ///   For the count of returned results see the <see cref="Results" /> collection.
  /// </summary>
  int Count { get; }

  /// <summary>
  ///   <see cref="System.Uri" /> for retrieving the next page of results.
  ///   <seealso cref="Previous" />
  /// </summary>
  string? Next { get; }

  /// <summary>
  ///   <see cref="System.Uri" /> for retrieving the previous page of results.
  ///   <seealso cref="Next" />
  /// </summary>
  string? Previous { get; }

  /// <summary>
  ///   The results on the current page. See <see cref="Next" /> and <see cref="Previous" /> for navigating through the pages.
  /// </summary>
  IEnumerable<T> Results { get; }
}
