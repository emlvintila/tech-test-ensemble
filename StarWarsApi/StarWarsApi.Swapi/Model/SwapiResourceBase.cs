﻿namespace StarWarsApi.Swapi.Model;

[Serializable]
public abstract class SwapiResourceBase
{
  protected SwapiResourceBase(string url, string created, string edited)
  {
    Url = url;
    Created = created;
    Edited = edited;
  }

  public string Url { get; }
  public string Created { get; }
  public string Edited { get; }
}
