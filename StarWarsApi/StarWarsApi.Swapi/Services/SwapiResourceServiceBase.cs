﻿using System.Net;
using Newtonsoft.Json;
using StarWarsApi.Swapi.Model;

namespace StarWarsApi.Swapi.Services;

public abstract class SwapiResourceServiceBase<TResource> : ISwapiResourceService<TResource>
{
  private readonly HttpClient httpClient;
  private readonly JsonSerializer jsonSerializer = JsonSerializer.Create();

  protected SwapiResourceServiceBase(IHttpClientFactory httpClientFactory) => httpClient = httpClientFactory.CreateClient();

  public async Task<TResource?> GetByIdAsync(int id, CancellationToken cancellationToken = default)
  {
    try
    {
      return await httpClient
        .GetAndDeserializeAsync<TResource>($"{GetBaseUrl()}{id}/", jsonSerializer, cancellationToken)
        .ConfigureAwait(false);
    }
    catch (HttpRequestException httpRequestException) when (httpRequestException.StatusCode is HttpStatusCode.NotFound)
    {
      return default;
    }
    catch (Exception e) when (e is JsonException or HttpRequestException)
    {
      throw new ServiceUnavailableException(null, e);
    }
  }

  public async Task<ISwapiResultsList<TResource>> GetPageAsync(int page = 1, string? searchQuery = null, CancellationToken cancellationToken = default)
  {
    try
    {
      return await httpClient
        .GetAndDeserializeAsync<SwapiResultsList<TResource>>($"{GetBaseUrl()}?page={page}&search={searchQuery}", jsonSerializer, cancellationToken)
        .ConfigureAwait(false);
    }
    catch (Exception e) when (e is JsonException or HttpRequestException)
    {
      throw new ServiceUnavailableException(null, e);
    }
  }

  public async Task<IEnumerable<ISwapiResultsList<TResource>>> GetAllAsync(int startingPage = 1, string? searchQuery = null,
    CancellationToken cancellationToken = default)
  {
    IList<ISwapiResultsList<TResource>> listOfLists = new List<ISwapiResultsList<TResource>>();

    try
    {
      ISwapiResultsList<TResource> list = await httpClient
        .GetAndDeserializeAsync<SwapiResultsList<TResource>>($"{GetBaseUrl()}?page={startingPage}&search={searchQuery}", jsonSerializer, cancellationToken)
        .ConfigureAwait(false);
      listOfLists.Add(list);

      while (list.Next is { } next)
      {
        list = await httpClient.GetAndDeserializeAsync<SwapiResultsList<TResource>>(next, jsonSerializer, cancellationToken);
        listOfLists.Add(list);
      }
    }
    catch (Exception e) when (e is JsonException or HttpRequestException)
    {
      throw new ServiceUnavailableException(null, e);
    }

    return listOfLists;
  }

  protected string GetBaseUrl() => $"https://swapi.dev/api/{GetResourceSubPath()}/";

  protected abstract string GetResourceSubPath();
}
