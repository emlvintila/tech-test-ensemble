﻿using StarWarsApi.Swapi.Model.Resources;

namespace StarWarsApi.Swapi.Services.Impl;

public class SwapiVehicleServiceImpl : SwapiResourceServiceBase<Vehicle>
{
  public SwapiVehicleServiceImpl(IHttpClientFactory httpClientFactory) : base(httpClientFactory) { }

  protected override string GetResourceSubPath() => "vehicles";
}
