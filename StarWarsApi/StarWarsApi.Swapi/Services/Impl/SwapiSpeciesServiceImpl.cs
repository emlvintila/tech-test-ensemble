﻿using StarWarsApi.Swapi.Model.Resources;

namespace StarWarsApi.Swapi.Services.Impl;

public class SwapiSpeciesServiceImpl : SwapiResourceServiceBase<Species>
{
  public SwapiSpeciesServiceImpl(IHttpClientFactory httpClientFactory) : base(httpClientFactory) { }

  protected override string GetResourceSubPath() => "species";
}
