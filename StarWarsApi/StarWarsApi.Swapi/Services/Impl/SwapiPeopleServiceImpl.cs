﻿using StarWarsApi.Swapi.Model.Resources;

namespace StarWarsApi.Swapi.Services.Impl;

public class SwapiPeopleServiceImpl : SwapiResourceServiceBase<People>
{
  public SwapiPeopleServiceImpl(IHttpClientFactory httpClientFactory) : base(httpClientFactory) { }

  protected override string GetResourceSubPath() => "people";
}
