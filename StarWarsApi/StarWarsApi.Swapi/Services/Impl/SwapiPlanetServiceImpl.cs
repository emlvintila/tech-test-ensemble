﻿using StarWarsApi.Swapi.Model.Resources;

namespace StarWarsApi.Swapi.Services.Impl;

public class SwapiPlanetServiceImpl : SwapiResourceServiceBase<Planet>
{
  public SwapiPlanetServiceImpl(IHttpClientFactory httpClientFactory) : base(httpClientFactory) { }

  protected override string GetResourceSubPath() => "planets";
}
