﻿using StarWarsApi.Swapi.Model.Resources;

namespace StarWarsApi.Swapi.Services.Impl;

public class SwapiFilmServiceImpl : SwapiResourceServiceBase<Film>
{
  public SwapiFilmServiceImpl(IHttpClientFactory httpClientFactory) : base(httpClientFactory) { }

  protected override string GetResourceSubPath() => "films";
}
