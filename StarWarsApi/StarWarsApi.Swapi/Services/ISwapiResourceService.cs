﻿using StarWarsApi.Swapi.Model;

namespace StarWarsApi.Swapi.Services;

public interface ISwapiResourceService<TResource>
{
  /// <summary>
  ///   Retrieves a <typeparamref name="TResource" /> by its id
  /// </summary>
  /// <param name="id"></param>
  /// <param name="cancellationToken"></param>
  /// <returns></returns>
  /// <exception cref="T:StarWarsApi.Swapi.ServiceUnavailableException"></exception>
  Task<TResource?> GetByIdAsync(int id, CancellationToken cancellationToken = default);

  /// <summary>
  ///   Retrieves all <typeparamref name="TResource" /> on <paramref name="page" />, filtered by <paramref name="searchQuery" />
  /// </summary>
  /// <param name="page"></param>
  /// <param name="searchQuery"></param>
  /// <param name="cancellationToken"></param>
  /// <returns></returns>
  /// <exception cref="T:StarWarsApi.Swapi.ServiceUnavailableException"></exception>
  Task<ISwapiResultsList<TResource>> GetPageAsync(int page = 1, string? searchQuery = null, CancellationToken cancellationToken = default);

  /// <summary>
  ///   Retrieves all <typeparamref name="TResource" /> starting from page <paramref name="startingPage" />, filtered by <paramref name="searchQuery" />
  /// </summary>
  /// <param name="startingPage"></param>
  /// <param name="searchQuery"></param>
  /// <param name="cancellationToken"></param>
  /// <returns></returns>
  /// <exception cref="T:StarWarsApi.Swapi.ServiceUnavailableException"></exception>
  Task<IEnumerable<ISwapiResultsList<TResource>>> GetAllAsync(int startingPage = 1, string? searchQuery = null, CancellationToken cancellationToken = default);
}
