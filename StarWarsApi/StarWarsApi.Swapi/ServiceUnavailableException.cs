﻿namespace StarWarsApi.Swapi;

public class ServiceUnavailableException : Exception
{
  public ServiceUnavailableException() : this("Service Unavailable") { }

  public ServiceUnavailableException(string? message) : base(message) { }

  public ServiceUnavailableException(string? message, Exception? innerException) : base(message, innerException) { }
}
