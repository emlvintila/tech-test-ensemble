﻿using System;
using NUnit.Framework;
using StarWarsApi.WebApi.Utils;

namespace StarWarsApi.WebApi.Test;

[TestFixture]
public class SwUriUtilsTest
{
  [Test]
  public void TestGetIdFromSwapiUriReturnsCorrectIdValue()
  {
    const int Id = 1;
    string uri = GetSwapiPeopleUri(Id);
    // ReSharper disable once InlineTemporaryVariable
    const int ExpectedResult = Id;

    Assert.AreEqual(ExpectedResult, SwUriUtils.GetIdFromSwapiUri(uri));
  }

  [Test]
  public void TestGetIdFromSwapiUriNegativeIdThrowsFormatException()
  {
    string uri = GetSwapiPeopleUri(-1);

    Assert.Throws<FormatException>(() => SwUriUtils.GetIdFromSwapiUri(uri));
  }

  [Test]
  public void TestGetIdFromSwapiUriNonNumericIdThrowsFormatException()
  {
    string uri = GetSwapiPeopleUri("NaN");

    Assert.Throws<FormatException>(() => SwUriUtils.GetIdFromSwapiUri(uri));
  }

  [Test]
  public void TestGetIdFromSwapiUriArbitraryProtocolAndDomainAndPathReturnsCorrectIdValue()
  {
    const int Id = 1;
    string uri = GetSwapiPeopleUri(Id);
    // ReSharper disable once InlineTemporaryVariable
    const int ExpectedResult = Id;

    foreach ((string search, string replace) in new[]
             {
               ("https", "arbitrary-protocol"),
               ("swapi.test", "example.com"),
               ("api/people", "5/a/7/c/-3")
             })
    {
      string modifiedUri = uri.Replace(search, replace);
      Assert.AreEqual(ExpectedResult, SwUriUtils.GetIdFromSwapiUri(modifiedUri));
    }
  }

  private static string GetSwapiPeopleUri(object id) => $"https://swapi.test/api/people/{id}/";
}
