﻿using System;
using NUnit.Framework;
using StarWarsApi.WebApi.Utils;

namespace StarWarsApi.WebApi.Test;

[TestFixture]
public class SwDateUtilsTest
{
  [Test]
  public void TestParseBirthYearUnknownLiteralReturnsNull()
  {
    Assert.IsNull(SwDateUtils.ParseYearWithEra("unknown"));
  }

  [Test]
  public void TestParseBirthYearBBYEraReturnsNegativeYear()
  {
    const int Year = 123;
    const string Era = "BBY";
    const int ExpectedResult = -Year;

    Assert.AreEqual(ExpectedResult, SwDateUtils.ParseYearWithEra($"{Year}{Era}"));
  }

  [Test]
  public void TestParseBirthYearABYEraReturnsPositiveYear()
  {
    const int Year = 123;
    const string Era = "ABY";
    // ReSharper disable once InlineTemporaryVariable
    const int ExpectedResult = Year;

    Assert.AreEqual(ExpectedResult, SwDateUtils.ParseYearWithEra($"{Year}{Era}"));
  }

  [Test]
  public void TestParseBirthYearOneWhitespaceCharacterBetweenYearAndEra()
  {
    const int Year = 123;
    const string Era = "ABY";
    // ReSharper disable once InlineTemporaryVariable
    const int ExpectedResult = Year;

    foreach (string whitespace in new[]
             {
               " ",
               "\t",
               "\n"
             })
    {
      Assert.AreEqual(ExpectedResult, SwDateUtils.ParseYearWithEra($"{Year}{whitespace}{Era}"));
    }
  }

  [Test]
  public void TestParseBirthYearNegativeYearThrowsFormatException()
  {
    Assert.Throws<FormatException>(() => SwDateUtils.ParseYearWithEra("-123BBY"));
    Assert.Throws<FormatException>(() => SwDateUtils.ParseYearWithEra("-123ABY"));
  }

  [Test]
  public void TestParseBirthYearInvalidEraThrowsFormatException()
  {
    Assert.Throws<FormatException>(() => SwDateUtils.ParseYearWithEra("123NonExistentEra"));
    Assert.Throws<FormatException>(() => SwDateUtils.ParseYearWithEra("123NEE"));
    Assert.Throws<FormatException>(() => SwDateUtils.ParseYearWithEra("123XBY"));
    Assert.Throws<FormatException>(() => SwDateUtils.ParseYearWithEra("123BY"));
  }

  [Test]
  public void TestParseBirthYearMoreThanOneSpaceThrowsFormatException()
  {
    string twoSpaces = new(' ', 2);
    string threeSpaces = new(' ', 3);
    string twoTabs = new('\t', 2);
    string twoNewlines = new('\n', 2);

    Assert.Throws<FormatException>(() => SwDateUtils.ParseYearWithEra($"123{twoSpaces}ABY"));
    Assert.Throws<FormatException>(() => SwDateUtils.ParseYearWithEra($"123{threeSpaces}ABY"));
    Assert.Throws<FormatException>(() => SwDateUtils.ParseYearWithEra($"123{twoTabs}ABY"));
    Assert.Throws<FormatException>(() => SwDateUtils.ParseYearWithEra($"123{twoNewlines}ABY"));
  }
}
